package com.formation.boris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BorisProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(BorisProjectApplication.class, args);
	}

}
